# Ruby Runner

Run Ruby files with one click! Useful for simple scripts etc.

![Overview](images/overview.gif)

## Features

It is possible to run the current Ruby file with one click using the `Ruby: Run file` run menu button.

![Button](images/button.png)

## Extension Settings

This extension contributes the following settings:

| Setting name | Description | Default value |
| ------------ | ----------- | ------------- |
| `ruby-runner.rubyRunCommand` | Specify the command which will be used to run Ruby files | `"ruby"` |
| `ruby-runner.railsRunCommand` | Specify the command which will be used to run Ruby files in Rails apps | `"bundle exec rails runner"` |

## Release Notes

### 0.2.0

Removed:

- Remove the legacy status bar button.
- Remove the unnecessary `ruby-runner.runRubyIcon` command

Changed:

- Change the name of the `ruby-runner.runCommand` setting to `ruby-runner.rubyRunCommand`

Added:

- Running files through Rails runner in projects with Rails installed through bundler
- New command `ruby-runner.runRails` for running ruby files with Rails runner
- New setting `ruby-runner.railsRunCommand`

### 0.1.1

Added a new `Ruby: Run file` button at the top of the editor (in run menu).

### 0.1.0

Initial Release.
It is possible to run the current Ruby file with one click using the `Run` Status Bar Button.
