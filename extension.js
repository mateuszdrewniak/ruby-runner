const vscode = require('vscode')

/**
 * @param {vscode.ExtensionContext} context
 */

let config
let context
let railsPrimaryVersion = 0

const railsGemfileLockRegex = /^\s*rails\s*\((\d+\.)+(\d|\w)+\)\s*$/
const runRubyId = 'ruby-runner.runRuby'
const runRailsId = 'ruby-runner.runRails'
const railsPrimaryVersionId = 'ruby-runner.railsPrimaryVersion'

function activate(ctxt) {
  context = ctxt
  vscode.commands.executeCommand('setContext', railsPrimaryVersionId, railsPrimaryVersion)
  console.log('Ruby Runner activated')
  config = vscode.workspace.getConfiguration('ruby-runner')
  parseRailsVersion()
}

function continueActivation() {
  vscode.commands.executeCommand('setContext', railsPrimaryVersionId, railsPrimaryVersion)
  context.subscriptions.push(vscode.commands.registerCommand(runRubyId, runCurrentRubyFile))
  context.subscriptions.push(vscode.commands.registerCommand(runRailsId, runCurrentFileRailsRunner))
}

function runCurrentRubyFile() {
  let terminal = vscode.window.activeTerminal
  let editor = vscode.window.activeTextEditor

  if(!editor || !editor.document) return

  if(!terminal) terminal = vscode.window.createTerminal()

  terminal.show()
  terminal.sendText(`${config.rubyRunCommand} ${editor.document.uri.path}`)
}

function runCurrentFileRailsRunner() {
  let terminal = vscode.window.activeTerminal
  let editor = vscode.window.activeTextEditor

  if(!editor || !editor.document) return

  if(!terminal) terminal = vscode.window.createTerminal()

  terminal.show()
  terminal.sendText(`${config.railsRunCommand} ${editor.document.uri.path}`)
}

function parseRailsVersion() {
  if(!vscode.workspace.workspaceFolders || !vscode.workspace.workspaceFolders[0]) return

  let workspaceRoot = vscode.workspace.workspaceFolders[0].uri

  // scan Gemfile.lock to get Rails version
  vscode.workspace.openTextDocument(vscode.Uri.joinPath(workspaceRoot, 'Gemfile.lock')).then((document) => {
    for (let i = 0; i < document.lineCount; i++) {
      let line = document.lineAt(i)
      let trimmedLine = line.text.trim().substring(0, 150)
      let regexMatch = railsGemfileLockRegex.exec(trimmedLine)

      if(regexMatch) {
        console.log(regexMatch[0])
        // `rails (6.11.3rc2)` => 6
        // `rails (4.1.2)` => 4
        railsPrimaryVersion = parseInt(regexMatch[0].replace('rails', '').replace('(', '').trim()[0])
      }
    }

    continueActivation()
  }, (_reason) => {
    continueActivation()
  })
}

function deactivate() {}

module.exports = {
  activate,
  deactivate
}
